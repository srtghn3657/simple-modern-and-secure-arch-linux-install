## Suggested packages to install:

_This is a list of useful and/or recommended packages for Arch Linux in general and KDE Plasma in particular. I personally prefer to just install plasma-desktop and the packages I actually need over installing the whole [plasma-meta](https://archlinux.org/packages/extra/any/plasma-meta/) package._

_This list also assumes, you've installed your system according to the [manual](https://gitlab.com/dataprolet/arch/-/blob/master/install.md) or using the [script](https://gitlab.com/dataprolet/arch/-/blob/master/README.md) included in this repository, which include the following packages already._

+ [base](https://archlinux.org/packages/core/any/base/)
+ [firefox](https://wiki.archlinux.org/title/Firefox)
+ [konsole](https://wiki.archlinux.org/title/Konsole)
+ [linux](https://wiki.archlinux.org/title/Kernel)
+ [linux-firmware](https://archlinux.org/packages/core/any/linux-firmware/)
+ [linux-zen](https://archlinux.org/packages/extra/x86_64/linux-zen/)
+ [lvm2](https://wiki.archlinux.org/title/LVM)
+ [nano](https://wiki.archlinux.org/title/Nano)
+ [networkmanager](https://wiki.archlinux.org/title/NetworkManager)
+ [plasma-desktop](https://wiki.archlinux.org/title/KDE#Plasma)
+ [sddm](https://wiki.archlinux.org/title/SDDM)
+ [sddm-kcm](https://archlinux.org/packages/extra/x86_64/sddm-kcm/)
+ [sudo](https://wiki.archlinux.org/title/Sudo)

The links will get you to the respective [Arch wiki](https://wiki.archlinux.org/) page if available or else to the [package's page](https://archlinux.org/packages/).

### Essential recommendations:
- [Microcode](https://wiki.archlinux.org/index.php/Microcode) (CPU manufacturers updates for processor microcode)
- [Reflector](https://wiki.archlinux.org/title/Reflector) (Updates pacman mirrorlist)
- [Zram-Generator](https://wiki.archlinux.org/title/Swap#zram-generator) (zram swap device tool)
- [Yay](https://github.com/Jguer/yay#installation) ([AUR](https://wiki.archlinux.org/title/Arch_User_Repository)-helper)

### KDE Plasma integrations:
* [breeze-gtk](https://archlinux.org/packages/extra/any/breeze-gtk/) (Breeze theme for GTK applications)
* [kde-gtk-config](https://archlinux.org/packages/extra/x86_64/kde-gtk-config/) (Adds graphical settings for GTK apps)  
* [kdeplasma-addons](https://archlinux.org/packages/extra/x86_64/kdeplasma-addons/) („All kind of addons to improve your Plasma experience.“)  
* [kscreen](https://archlinux.org/packages/extra/x86_64/kscreen/) (Adds screen section to graphical settings)
* [plasma-browser-integration](https://archlinux.org/packages/extra/x86_64/plasma-browser-integration/)
* [plasma-nm](https://archlinux.org/packages/extra/x86_64/plasma-nm/) (Network manager applet)
* [plasma-pa](https://archlinux.org/packages/extra/x86_64/plasma-pa/) (Audio manager applet)
* [powerdevil](https://archlinux.org/packages/extra/x86_64/powerdevil/) (Adds energy manager to graphical settings)

##### Noteworthy but negligible:
- [keditbookmarks](https://archlinux.org/packages/extra/x86_64/keditbookmarks/) (Optional, tool for edting file-browser bookmarks)
- [plasma5-applets-thermal-monitor](https://archlinux.org/packages/community/any/plasma5-applets-thermal-monitor/) (Thermal monitor applet)

### KDE Utilities:
+ [ark](https://archlinux.org/packages/extra/x86_64/ark/) (GUI archive tool)
+ [bluedevil](https://archlinux.org/packages/extra/x86_64/bluedevil/) (Bluetooth integration)
+ [dolphin](https://wiki.archlinux.org/title/Dolphin) (File browser)
+ [filelight](https://archlinux.org/packages/extra/x86_64/filelight/)] (Graphical disk usage viewer)
+ [gwenview](https://archlinux.org/packages/extra/x86_64/gwenview/) (Image viewer)
+ [kate](https://archlinux.org/packages/extra/x86_64/kate/) (Text editor)
+ [kcalc](https://archlinux.org/packages/extra/x86_64/kcalc/) (Calculator)
+ [kdeconnect](https://wiki.archlinux.org/title/KDE#KDE_Connect) [**Recommended**] (Smartphone intergration)
+ [kinfocenter](https://archlinux.org/packages/extra/x86_64/kinfocenter/) (Shows system information)
+ [kmag](https://archlinux.org/packages/extra/x86_64/kmag/) (Screen magnifier)
+ [kompare](https://archlinux.org/packages/extra/x86_64/kompare/) (File difference tool)
+ [konsole](https://wiki.archlinux.org/title/Konsole) (Terminal emulator)
+ [konversation](https://archlinux.org/packages/extra/x86_64/konversation/) (GUI IRC client)
+ [krename](https://archlinux.org/packages/community/x86_64/krename/) (Multi-file renamer)
+ [krusader](https://archlinux.org/packages/community/x86_64/krusader/) [**Recommended**] (Twin-panel file browser)
+ [plasma-systemmonitor](https://archlinux.org/packages/extra/x86_64/plasma-systemmonitor/) (Ressource monitor)
+ [ksystemlog](https://archlinux.org/packages/extra/x86_64/ksystemlog/) (Graphical system log viewer)
+ [okular](https://archlinux.org/packages/extra/x86_64/okular/) (PDF viewer)
+ [partitionmanager](https://archlinux.org/packages/extra/x86_64/partitionmanager/) (Graphical partition manager)
+ [spectacle](https://archlinux.org/packages/extra/x86_64/spectacle/) (Graphical screenshot tool)
+ [yakuake](https://wiki.archlinux.org/title/Yakuake) (Drop-down terminal)

### Software and tools: 

#### Graphical:
- [Audacity](https://archlinux.org/packages/community/x86_64/audacity/)/[Tenacity (AUR)](https://aur.archlinux.org/packages/tenacity-git) (Audio editor)
- [BackInTime (AUR)](https://wiki.archlinux.org/title/Back_In_Time)¹ (Simple backup tool)
- [Discord](https://wiki.archlinux.org/title/Discord) (Voice chat application)
- [Element](https://archlinux.org/packages/community/x86_64/element-desktop/) (Matrix client)
- [GIMP](https://wiki.archlinux.org/title/GIMP) (Raster image editor)
- [Imagewriter (AUR)](https://aur.archlinux.org/packages/imagewriter) (Image to USB writer)
- [JDownloader2 (AUR)](https://wiki.archlinux.org/title/JDownloader) (Download manager)
- [KeePassXC](https://archlinux.org/packages/community/x86_64/keepassxc/)¹ (Password database)
- [Kodi](https://wiki.archlinux.org/title/Kodi) (Entertainment hub)
- [LibreOffice](https://wiki.archlinux.org/title/LibreOffice)¹ (Productivity suite)
- [MediaInfo (GUI)](https://archlinux.org/packages/community/x86_64/mediainfo-gui/) (Media information viewer)
- [Nextcloud-Client](https://archlinux.org/packages/community/x86_64/nextcloud-client/) (Client for Nextcloud)
- [OBS-Studio](https://wiki.archlinux.org/title/Open_Broadcaster_Software) (Video recording software)
- [Octopi (AUR)](https://aur.archlinux.org/packages/octopi) (Graphical package manager)
- [Octopi-Notifier-Qt5 (AUR)](https://aur.archlinux.org/packages/octopi-notifier-qt5) (Update notifier)
- [Rhythmbox](https://wiki.archlinux.org/title/Rhythmbox) (Music player and iPod manager)
- [Signal-Desktop](https://archlinux.org/packages/community/x86_64/signal-desktop/) (Secure messager client)
- [Soundkonverter](https://archlinux.org/packages/community/x86_64/soundkonverter/) (Audio file converter)
- [Soundwire (AUR)](https://aur.archlinux.org/packages/soundwire) (Streams audio over network)
- [StandardNotes (AUR)](https://aur.archlinux.org/packages/standardnotes-bin)¹ (Secure note app client)
- [Teamviewer (AUR)](https://aur.archlinux.org/packages/teamviewer) (Remote desktop application)
- [Telegram-Desktop](https://wiki.archlinux.org/title/Telegram) (Messanger client)
- [Thunderbird](https://wiki.archlinux.org/title/Thunderbird)¹ (E-Mail application)
- [TimeShift (AUR)](https://aur.archlinux.org/packages/timeshift)¹ (Snapshot application)
- [Tor-Browser (AUR)](https://aur.archlinux.org/packages/tor-browser) (The onion router browser)
- [Virt-Manager](https://archlinux.org/packages/community/any/virt-manager/) (Virtual maschine manager)

#### Command-line or additional:
+ [fwupd](https://wiki.archlinux.org/title/Fwupd) (Firmware update daemon)
+ [git](https://wiki.archlinux.org/title/Git) (Version control system)
+ [hdparm](https://wiki.archlinux.org/title/Hdparm) (Hard disk utility)
+ [hunspell](https://wiki.archlinux.org/title/Language_checking#Spell_checkers) (Spell checker)
+ [hyphen](https://wiki.archlinux.org/title/LibreOffice#Hyphenation_rules) (Hyphenation rules for LibreOffice)
+ [inxi (AUR)](https://aur.archlinux.org/packages/inxi) (System information tool)
+ [libvirt](https://wiki.archlinux.org/title/Libvirt) Software collection for virtual machine management)
+ [neofetch](https://archlinux.org/packages/community/any/neofetch/) (System information tool)
+ [networkmanager-openconnect](https://wiki.archlinux.org/title/OpenConnect#NetworkManager) (NetworkManager intergration for OpenConnect)
+ [pacman-contrib](https://archlinux.org/packages/community/x86_64/pacman-contrib/) (Additional tools for pacman)
+ [PipeWire](https://wiki.archlinux.org/title/PipeWire) (New multimedia framework)
+ [pkgstats](https://wiki.archlinux.org/title/Pkgstats) (Sends installed packages to Arch developers)
+ [powertop](https://wiki.archlinux.org/title/Powertop)¹ (Intel powersaving tool)
+ [qemu](https://wiki.archlinux.org/title/QEMU) (Emulator and virtualizer)
+ [rmtrash (AUR)](https://wiki.archlinux.org/title/Trash_management)¹ (Freedesktop.org compatible trash management)
+ [thermald](https://archlinux.org/packages/community/x86_64/thermald/)² (Linux Thermal Control Daemon)
+ [TLP](https://wiki.archlinux.org/title/TLP)² (Laptop battery power saving utility)
+ [Uncomplicated firewall](https://wiki.archlinux.org/title/Uncomplicated_Firewall)¹ (Manager for netfilter firewall back-ends)
+ [xsuspender (AUR)](https://aur.archlinux.org/packages/xsuspender-git)² (Suspends inactive X11 applications)

- [unrar](https://archlinux.org/packages/?name=unrar), [unzip](https://archlinux.org/packages/?name=unzip), [p7zip](https://archlinux.org/packages/?name=p7zip) ([Archiving and compression tools](https://wiki.archlinux.org/title/Archiving_and_compression#Archiving_and_compression))

¹: Recommended essentials.
²: Specifically useful for laptops.

### Further tips:
- Point to this: https://gitlab.com/dataprolet/random-linux-tips
