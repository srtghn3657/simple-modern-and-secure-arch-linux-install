# Simple, modern and secure installation of Arch Linux with KDE Plasma.

Moved to https://git.dataprolet.de/dataprolet/Arch-Linux-Installer.

### This is my personal installation routine, which I continously expand and redefine.  

This installation script aims for a simple (read minimal) Arch Linux installation, which is modern in the way that it uses UEFI, systemd-boot and Wayland, secure in the way that it uses full-disk encryption using LUKS as well as flexibility by using Btrfs subvolumes for the root and home partition.  

Tested and working as of **May 2024**.

![Post-install screenshot.](screenshot.png)

## To automatically install Arch Linux, clone this repository:
```
$ pacman -Sy git
$ git clone https://gitlab.com/dataprolet/arch
$ cd arch
$ sh install.sh
```

### Or as a one-liner: 
```
# pacman -Sy git --noconfirm && git clone https://gitlab.com/dataprolet/arch && cd arch && sh install.sh
```

### To switch from X11 to Wayland on an existing installation use the following command and reboot: 
```
# pacman -S wayland xorg-xwayland && sed -i 's/plasma/plasmawayland/g' /etc/sddm.conf.d/kde_settings.conf
```

### *There are now two additional versions of the script:*
- `install-lvm.sh` - Installs using Ext4 and uses LVM for /root and /home.
- `install-grub.sh` - Installs using GRUB for legacy BIOS (and Btrfs).
- `ìnstall-x11.sh`- Installs using X11 (and systemd-boot/Btrfs).

## All features:
- Prompt for all user input at the start (hostname, username, passwords).
- Partitions a primary and a boot partition.
- Encrypts the primary partition using LUKS.
- Creates Btrfs filesystems with zstd compression.
- Creates Btrfs volumes for /root and /home.
- Refreshes the latest mirror list.
- Installs a base system (base linux linux-firmware btrfs-progs nano networkmanager sudo).
- Installs very few additional packages (firefox konsole linux-zen)
- Installs a minimal KDE Plasma desktop environment (plasma-desktop, sddm, sdmd-kcm)
- Installs systemd-boot as a bootloader.
- Sets German keyboard layout and English locale.
- Enables SDDM auto-login to boot into Plasma after the first reboot.

---
