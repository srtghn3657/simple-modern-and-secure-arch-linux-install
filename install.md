# Simple, modern and secure installation of Arch Linux with KDE Plasma.
  
_If you won't run into any issues, this should take you under an hour from nothing to a graphical interface. Good luck!_

**Load the preferred keyboard. In my case it's "de" for Germany.**  
$ loadkeys de  
  
**List devices to make sure you partition the right device.**  
$ lsblk  
  
**Partition the device using gdisk (assuming /dev/sda).**  
$ gdisk /dev/sda  
  
**Create GPT and EFI-partion.**  
_Creates new GPT-partition table._  
$ o  
**Create LVM logical volumes for /root and /home.  
Your root volume should be 25-50 GB big, your home volume takes up the remaining space.**  
$ lvcreate -L 25G -n root main  
$ lvcreate -l 100%FREE -n home main
  
**Create the filesystems and mount your volumes.**  
$ mkfs.fat -F 32 -n UEFI /dev/sda1  
_-F specifies the type of file allocation tables used (12, 16 or 32 bit).  
-n sets the volume name (label) of the filesystem to "UEFI"._
  
  
**Create ext4 filesystem on root and home volumes.**  
$ mkfs.ext4 -L root /dev/mapper/main-root  
$ mkfs.ext4 -L home /dev/mapper/main-home
  
**Create mountpoints and mount partitions.**  
$ mount /dev/mapper/main-root /mnt  
$ mkdir /mnt/boot  
$ mkdir /mnt/home  
$ mount /dev/sda1 /mnt/boot  
$ mount /dev/mapper/main-home /mnt/home  
  
**Optionally update mirrorlist using reflector.**  
_In this case use the latest 10 mirrors from Germany, that have been synchronized within the last 24 hours and sort the by download-rate and write them to the pacman mirrorlist._  
$  reflector --verbose --latest 10 --country Germany --age 24 --protocol https --sort rate --save /etc/pacman.d/mirrorlist  
  
**Install base system and basic packages (network, editor, sudo).**  
**The package base-devel is not necessary but recommended.**  
$ pacstrap /mnt base base-devel linux linux-firmware networkmanager nano sudo lvm2  
  
**Optional: install all basic packages for KDE Plasma:**  
 $ pacstrap /mnt base base-devel linux linux-zen linux-firmware networkmanager nano sudo lvm2 sddm sddm-kcm plasma-desktop konsole firefox  
  
**Generate fstab.**  
$ genfstab -Up /mnt > /mnt/etc/fstab  
_-U = Use UUID for source identifiers.  
-p = Exclude pseudofs mounts (default behavior)._  
  
**Chroot into your installation.**  
$ arch-chroot /mnt  
  
**Set your hostname.**  
$ echo $host > /etc/hostname  
  
**Generate locale.gen for your preferred language (and preferribly english).**  
$ nano /etc/locale.gen  
_Uncomment your language, e.g. de_DE.UTF-8 UTF-8 (and en_US.UTF-8)._  
  
**Generate the locale.**  
$ locale-gen  
_The uncommented languages should appear followed by "done"._  
  
echo KEYMAP=de-latin1 > /etc/vconsole.conf  
echo LANG=de_DE.UTF-8 > /etc/locale.conf  
  
**Add necessary HOOKS and MODULES.**  
$ nano /etc/mkinitcpio.conf  
  
```
MODULES=(ext4)  
HOOKS=(base udev autodetect modconf block keyboard keymap encrypt lvm2 filesystems fsck shutdown)
```
  
**Create mkinitcpio.**  
$ mkinitcpio -p linux-zen  
  
**Enable NetworkManager and SDDM to start on next boot.**  
$ systemctl enable NetworkManager sddm  
  
**Set a root-password.**  
$ passwd  
  
**Install systemd-boot bootloader.**  
$ bootctl install  
  
**Create the bootloader config.**  
$ nano /boot/loader/entries/arch.conf  
  
**Add the following:**  
```
title    Arch Linux
linux    /vmlinuz-linux-zen
initrd   /initramfs-linux-zen.img
options   cryptdevice=/dev/sda2:main root=/dev/mapper/main-root rw  
options   init=/usr/lib/systemd/systemd  
options   lang=de locale=de_DE.UTF-8  
```
  
**Create a fallback config.**  
$ cp /boot/loader/entries/arch.conf /boot/loader/entries/arch-fallback.conf  
  
**Edit the fallback config.**  
$ nano /boot/loader/entries/arch-fallback.conf  
  
**Change it to the following:**  
```
title    Arch Linux Fallback
linux    /vmlinuz-linux
initrd   /initramfs-linux-fallback.img
```
  
**Edit loader config.**  
$ nano /boot/loader/loader.conf  
  
**Change it to the following:**  
```
timeout 1
default arch.conf
```
  
**Set your keyboard to your language.**  
$ localectl --no-convert set-keymap de-latin1-nodeadkeys    
  
**Create a user with a /home-directory.**  
$ useradd -m $myusername  
  
**Set password for your user.**  
$ passwd $myusername  
  
**Add $myusername to group wheel for sudo access.**  
$ gpasswd -a $myusername wheel  
  
**Edit sudoers-file for sudo access.**  
$ nano /etc/sudoers  
  
**Uncomment „%wheel ALL=(ALL) ALL“.**  
**Don't forget to also delete the space between the # and %wheel.**  

**Since it's an encrypted single-user system, we don't need a login screen after boot.**  
**Create the following directory and config-file for sddm:**  

$ mkdir /etc/sddm.conf.d
$ nano /etc/sddm.conf.d/autologin.conf

```
[Autologin]
User=john
Session=plasma
```

**Exit and reboot.**  
$ exit  
$ umount /mnt/boot  
$ umount /mnt/home  
$ reboot    
  
**Now you are logged in to your Plasma desktop environment and ready to configure and use your Arch Linux.**
