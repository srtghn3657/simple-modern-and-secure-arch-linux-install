#!/bin/bash

# This script installs additional packages and configures the system

# Installing packages

# Installing AUR-helper Yay
git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin
makepkg -si --noconfirm
cd ~

# Clone repo containing package list
git clone https://gitlab.com/dataprolet/arch

# Enable multilib repo
sudo sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf

# Enabling AHLP.GO repo

# Configure compiler
sudo sed -E -i 's/-march=(\S*)/-march=native/' /etc/makepkg.conf
sudo sed -E -i 's/-mtune==(\S*)/-mtune=native/' /etc/makepkg.conf
sudo sed -E -i 's/-O2/-O3/' /etc/makepkg.conf
sudo sed -E -i 's/#RUSTFLAGS=(.*)$/RUSTFLAGS="-C opt-level=2 -C target-cpu=native"/' /etc/makepkg.conf
sudo sed -E -i 's/#MAKEFLAGS=(.*)$/MAKEFLAGS="-j$(nproc)"/' /etc/makepkg.conf

# Stupid workaround for package conflict
yes | yay -S pipewire-jack

# Install packages
yay -Syu --needed --noconfirm - < arch/pkgs

# Stupid workaround for package conflict
yes | yay -S wine-ge-custom

---

# Enable microcode in bootloader config

# Enable dbus-broker

# Enable irqbalance

# Enable pipewire

# Enable pkgstats

# Enable paccache timer

# Configure reflector
# Enable reflector timer

# Configure and enable ufw

# Configure and enable zram-generator

# Enable SSH
systemctl enable sshd

# Copy systemd-timer

# Copy pacman hooks

# Copy bashrc

# Change journald size

# Enable trim and timer

# enable parallel downloads for pacman

# Change I/O scheduler